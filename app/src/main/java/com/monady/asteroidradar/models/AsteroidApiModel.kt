package com.monady.asteroidradar.models

import com.google.gson.annotations.SerializedName


data class AsteroidApiModel(
    @SerializedName("near_earth_objects") val nearEarthObjects: Map<String, List<NEObjects>>
)

data class NEObjects(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val codename: String,
    @SerializedName("absolute_magnitude_h") val absoluteMagnitude: Double,
    @SerializedName("estimated_diameter") val estimatedDiameter: EstimatedDiameter,
    @SerializedName("is_potentially_hazardous_asteroid") val isHazardous: Boolean,
    @SerializedName("close_approach_data") val closeApproachData: List<CloseApproachData>
)

data class EstimatedDiameter(
    @SerializedName("kilometers") val kilometers: Kilometers
)

data class Kilometers(
    @SerializedName("estimated_diameter_max") val estimatedDiameterMax: Double
)

data class CloseApproachData(
    @SerializedName("close_approach_date") val closeApproachDate: String,
    @SerializedName("relative_velocity") val relativeVelocity: RelativeVelocity,
    @SerializedName("miss_distance") val missDistance: MissDistance
)

data class RelativeVelocity(
    @SerializedName("kilometers_per_second") val kilometersPerSecond: Double
)

data class MissDistance(
    @SerializedName("astronomical") val astronomical: Double
)

fun AsteroidApiModel.toAsteroids(date: String): List<Asteroid> {

    val list = mutableListOf<Asteroid>()

    val data = nearEarthObjects[date]

    lateinit var closeData: CloseApproachData

    data?.let{
        data.forEach{
            it.closeApproachData.forEach{
                if (it.closeApproachDate == date) {
                    closeData = it
                }
            }
            list.add(
                Asteroid(
                    id = it.id,
                    codename = it.codename,
                    closeApproachDate = closeData.closeApproachDate,
                    absoluteMagnitude = it.absoluteMagnitude,
                    estimatedDiameter = it.estimatedDiameter.kilometers.estimatedDiameterMax,
                    relativeVelocity = closeData.relativeVelocity.kilometersPerSecond,
                    distanceFromEarth = closeData.missDistance.astronomical,
                    isPotentiallyHazardous = it.isHazardous
                )
            )
        }
    }
    return list
}
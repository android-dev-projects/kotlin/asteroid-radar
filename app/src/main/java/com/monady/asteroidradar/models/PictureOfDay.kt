package com.monady.asteroidradar.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "potd")
data class PictureOfDay(@SerializedName("media_type") val mediaType: String,
                        @SerializedName("title") val title: String,
                        @PrimaryKey @SerializedName("url") val url: String,
                        @SerializedName("date") val pictureDate: String)

package com.monady.asteroidradar.viewmodel

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import com.monady.asteroidradar.models.Asteroid
import com.monady.asteroidradar.repository.AsteroidsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(app: Application) : AndroidViewModel(app) {

    private val _navigateToAsteroidDetails = MutableLiveData<Long?>()
    val navigateToAsteroidDetails: LiveData<Long?>
        get() = _navigateToAsteroidDetails


    private val _asteroidDetails = MutableLiveData<Asteroid>()
    val asteroidDetails: LiveData<Asteroid>
        get() = _asteroidDetails

    private val _listLoadingStatus = MutableLiveData<Boolean>()
    val listLoadingStatus: LiveData<Boolean>
        get() = _listLoadingStatus

    private val _potdLoadingStatus = MutableLiveData<Boolean>()
    val potdLoadingStatus: LiveData<Boolean>
        get() = _potdLoadingStatus

    private val repo = AsteroidsRepository.getDataRepository(app)

    val asteroids = liveData {
        emitSource(repo.getAllAsteroids())
    }

    val potd = liveData {
        emitSource(repo.getPotd())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun loadData() {
        viewModelScope.launch {
            _listLoadingStatus.postValue(true)
            repo.loadAsteroidsData()
            _listLoadingStatus.postValue(false)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun loadPictureOfTheDay(){
        viewModelScope.launch {
            _potdLoadingStatus.postValue(true)
            repo.getImageOfTheDay()
            _potdLoadingStatus.postValue(false)
        }
    }

    fun beginNavigateToAsteroidDetails(asteroidId: Long) {
        _navigateToAsteroidDetails.postValue(asteroidId)
    }

    fun endNavigateToAsteroidDetails() {
        _navigateToAsteroidDetails.value = null
    }

    fun getAsteroidDetails(asteroidId: Long) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _asteroidDetails.postValue(repo.getAsteroidById(asteroidId))
            }
        }
    }
}
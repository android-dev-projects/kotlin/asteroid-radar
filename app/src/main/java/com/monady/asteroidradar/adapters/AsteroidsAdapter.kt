package com.monady.asteroidradar.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.monady.asteroidradar.models.Asteroid
import com.monady.asteroidradar.databinding.AsteroidCardBinding
import com.monady.asteroidradar.utils.AsteroidListener
import com.monady.asteroidradar.utils.AsteroidsDiffUtil

class AsteroidsAdapter(private val asteroidListener: AsteroidListener) :
    ListAdapter<Asteroid, AsteroidsAdapter.AsteroidsViewHolder>(AsteroidsDiffUtil()) {

    class AsteroidsViewHolder(private val binding: AsteroidCardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): AsteroidsViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = AsteroidCardBinding.inflate(layoutInflater, parent, false)
                return AsteroidsViewHolder(binding)
            }
        }

        fun bind(asteroid: Asteroid, asteroidListener: AsteroidListener) {
            binding.asteroid = asteroid
            binding.asteroidClickListener = asteroidListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AsteroidsViewHolder {
        return AsteroidsViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: AsteroidsViewHolder, position: Int) {
        holder.bind(getItem(position), asteroidListener)
    }
}
package com.monady.asteroidradar.repository

import android.app.Application
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.monady.asteroidradar.models.Asteroid
import com.monady.asteroidradar.api.NasaApi
import com.monady.asteroidradar.models.toAsteroids
import com.monady.asteroidradar.room.AsteroidsDatabase
import com.monady.asteroidradar.utils.Constants.API_QUERY_DATE_FORMAT
import com.monady.asteroidradar.utils.Constants.MEDIA_TYPE
import com.monady.asteroidradar.utils.DateUtilities.Companion.getCurrentDate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class AsteroidsRepository(app: Application) {

    companion object {
        private lateinit var repoInstance: AsteroidsRepository
        fun getDataRepository(app: Application): AsteroidsRepository {
            synchronized(this) {
                if (!::repoInstance.isInitialized) {
                    repoInstance = AsteroidsRepository(app)
                }
                return repoInstance
            }
        }
    }

    private val asteroidsDao = AsteroidsDatabase.getInstance(app.applicationContext).asteroidsDao
    private val potdDao = AsteroidsDatabase.getInstance(app.applicationContext).potdDao

    fun getAllAsteroids() = asteroidsDao.getAllAsteroids(getCurrentDate())

    fun getPotd() = potdDao.getPictureOfDay()

    suspend fun getAsteroidById(asteroidId: Long) = asteroidsDao.getAsteroidById(asteroidId)

    suspend fun insertAsteroids(asteroids: List<Asteroid>) {
        withContext(Dispatchers.IO) {
            asteroidsDao.insertAsteroids(asteroids)
        }
    }

    suspend fun insertAsteroid(asteroid: Asteroid) {
        withContext(Dispatchers.IO) {
            asteroidsDao.insertAsteroid(asteroid)
        }
    }

    suspend fun checkExist(asteroidId: Long): Boolean {
        var state: Boolean
        withContext(Dispatchers.IO) {
            state = asteroidsDao.checkAsteroidExists(asteroidId)
        }
        return state
    }

    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun loadAsteroidsData() {

        try {
            val currentDate = getCurrentDate(1)

            val asteroidsLists =
                NasaApi.apiService.getAsteroids(startDate = currentDate, endDate = currentDate)
                    .toAsteroids(currentDate)

            asteroidsLists.forEach {
                if (!checkExist(it.id)) {
                    insertAsteroid(it)
                }
            }
        } catch (ex: Exception) {

        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun getImageOfTheDay(){
        val currentDate = getCurrentDate()

        if(!potdDao.checkPictureOfDayExists())
        {
            val imageOftheDay = NasaApi.apiService.getPhotoOfTheDay(date = currentDate)
            potdDao.insertPictureOfDay(imageOftheDay)
        }
        else{
            val currentPotd = getPotd().value
            currentPotd?.let{
                if(it.pictureDate != currentDate){
                    val imageOftheDay = NasaApi.apiService.getPhotoOfTheDay(date = currentDate)
                    if(imageOftheDay.mediaType == MEDIA_TYPE){
                        potdDao.deletePictureOfDay()
                        potdDao.insertPictureOfDay(imageOftheDay)
                    }
                }
            }
        }

    }
}
package com.monady.asteroidradar.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.monady.asteroidradar.models.PictureOfDay

@Dao
interface PotdDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPictureOfDay(potd: PictureOfDay)

    @Query("Select * from potd")
    fun getPictureOfDay(): LiveData<PictureOfDay>

    @Query("Delete from potd")
    suspend fun deletePictureOfDay()

    @Query("SELECT EXISTS(SELECT * FROM potd)")
    suspend fun checkPictureOfDayExists(): Boolean
}
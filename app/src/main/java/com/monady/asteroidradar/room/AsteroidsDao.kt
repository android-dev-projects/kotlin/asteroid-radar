package com.monady.asteroidradar.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.monady.asteroidradar.models.Asteroid

@Dao
interface AsteroidsDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAsteroids(asteroids: List<Asteroid>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAsteroid(asteroid: Asteroid)

    @Update
    suspend fun updateAsteroid(asteroid: Asteroid)

  /*  @Query("Select * from asteroids_table")
    fun getAllAsteroids(): LiveData<List<Asteroid>>*/

    @Query("Select * from asteroids_table where closeApproachDate = :date")
    fun getAllAsteroids(date: String): LiveData<List<Asteroid>>

    @Query("Delete from asteroids_table")
    suspend fun deleteAllAsteroids()

    @Query("Delete from asteroids_table where id = :asteroidId")
    suspend fun deleteAsteroidById(asteroidId: Int)

    @Query("select * from asteroids_table where id = :asteroidId")
    suspend fun getAsteroidById(asteroidId: Long): Asteroid

    @Query("SELECT EXISTS(SELECT * FROM asteroids_table WHERE id = :asteroidId)")
    suspend fun checkAsteroidExists(asteroidId: Long): Boolean


}
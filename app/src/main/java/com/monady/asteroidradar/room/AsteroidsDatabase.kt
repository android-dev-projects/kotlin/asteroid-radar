package com.monady.asteroidradar.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.monady.asteroidradar.models.Asteroid
import com.monady.asteroidradar.models.PictureOfDay

@Database(entities = [Asteroid::class, PictureOfDay::class], version = 1)
abstract class AsteroidsDatabase : RoomDatabase() {

    abstract val asteroidsDao: AsteroidsDao
    abstract val potdDao: PotdDao

    companion object {
        private lateinit var INSTANCE: AsteroidsDatabase

        fun getInstance(context: Context): AsteroidsDatabase {
            synchronized(this) {
                if (!::INSTANCE.isInitialized) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AsteroidsDatabase::class.java,
                        "asteroids_database"
                    ).build()
                    return INSTANCE
                }
            }
            return INSTANCE
        }
    }
}
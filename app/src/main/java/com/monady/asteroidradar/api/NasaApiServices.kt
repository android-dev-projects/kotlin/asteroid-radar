package com.monady.asteroidradar.api

import com.monady.asteroidradar.utils.Constants.API_KEY
import com.monady.asteroidradar.models.AsteroidApiModel
import com.monady.asteroidradar.models.PictureOfDay
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NasaApiServices {
    @GET("neo/rest/v1/feed")
    suspend fun getAsteroids(@Query("api_key") apiKey: String = API_KEY,
    @Query("start_date") startDate: String, @Query("end_date") endDate: String) : AsteroidApiModel

    @GET("/neo/rest/v1/neo/{asteroid_id}")
    suspend fun getAsteroidById(@Path("asteroid_id") asteroidId: Int = 2516155,
    @Query("api_key") apiKey: String = API_KEY): AsteroidApiModel

    @GET("planetary/apod")
    suspend fun getPhotoOfTheDay(@Query("api_key") apiKey: String = API_KEY,
    @Query("date") date: String): PictureOfDay

}




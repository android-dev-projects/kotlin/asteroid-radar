package com.monady.asteroidradar.api

import com.monady.asteroidradar.utils.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NasaApi{

    /*private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()*/

    private val retrofit = Retrofit.Builder()
        //.addConverterFactory(MoshiConverterFactory.create(moshi))
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(Constants.BASE_URL)
        .build()

    val apiService: NasaApiServices by lazy{
        retrofit.create(NasaApiServices::class.java)
    }
}
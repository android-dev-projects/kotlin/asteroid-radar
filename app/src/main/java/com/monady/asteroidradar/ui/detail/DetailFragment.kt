package com.monady.asteroidradar.ui.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.monady.asteroidradar.R
import com.monady.asteroidradar.databinding.FragmentDetailBinding
import com.monady.asteroidradar.viewmodel.MainViewModel
import com.monady.asteroidradar.viewmodel.ViewModelFactory

class DetailFragment : Fragment() {

    lateinit var binding: FragmentDetailBinding

    private val viewModel: MainViewModel by lazy {
        val app = requireNotNull(this.activity).application

        val viewModelFactory = ViewModelFactory(app)

        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this

        val asteroidId = DetailFragmentArgs.fromBundle(
            requireArguments()
        ).asteroidId

        viewModel.getAsteroidDetails(asteroidId)

        viewModel.asteroidDetails.observe(viewLifecycleOwner, Observer{
            it?.let{
                binding.asteroid = it
            }
        })

        binding.helpButton.setOnClickListener {
            displayAstronomicalUnitExplanationDialog()
        }

        return binding.root
    }

    private fun displayAstronomicalUnitExplanationDialog() {
        val builder = AlertDialog.Builder(requireActivity()!!)
            .setMessage(getString(R.string.astronomica_unit_explanation))
            .setPositiveButton(android.R.string.ok, null)
        builder.create().show()
    }
}
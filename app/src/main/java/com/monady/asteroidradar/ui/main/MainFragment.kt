package com.monady.asteroidradar.ui.main

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.monady.asteroidradar.R
import com.monady.asteroidradar.adapters.AsteroidsAdapter
import com.monady.asteroidradar.databinding.FragmentMainBinding
import com.monady.asteroidradar.utils.AsteroidListener
import com.monady.asteroidradar.viewmodel.MainViewModel
import com.monady.asteroidradar.viewmodel.ViewModelFactory
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_main.view.*

class MainFragment : Fragment() {

    private val viewModel: MainViewModel by lazy {
        val app = requireNotNull(this.activity).application

        val viewModelFactory = ViewModelFactory(app)

        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }

    lateinit var binding: FragmentMainBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMainBinding.inflate(layoutInflater)

        binding.lifecycleOwner = this

        viewModel.potd.observe(viewLifecycleOwner, Observer{
            it?.let {
                Picasso.with(requireContext()).load(it.url).into(binding.activityMainImageOfTheDay)
                binding.potd = it
            }
        })

        viewModel.loadData()
        viewModel.loadPictureOfTheDay()

        binding.asteroidRecycler.adapter = AsteroidsAdapter(AsteroidListener { asteroidId ->
            viewModel.beginNavigateToAsteroidDetails(asteroidId)
        })

        binding.viewModel = viewModel

        viewModel.navigateToAsteroidDetails.observe(viewLifecycleOwner, Observer {
            it?.let {
                findNavController().navigate(
                    MainFragmentDirections.actionShowDetail(
                        it
                    )
                )
                viewModel.endNavigateToAsteroidDetails()
            }
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_overflow_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return true
    }
}

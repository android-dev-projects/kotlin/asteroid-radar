package com.monady.asteroidradar.utils

import androidx.recyclerview.widget.DiffUtil
import com.monady.asteroidradar.models.Asteroid

class AsteroidsDiffUtil : DiffUtil.ItemCallback<Asteroid>() {
    override fun areItemsTheSame(oldItem: Asteroid, newItem: Asteroid): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Asteroid, newItem: Asteroid): Boolean {
        return oldItem == newItem
    }
}
package com.monady.asteroidradar.utils

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DateUtilities {
    companion object{
        @RequiresApi(Build.VERSION_CODES.O)
        fun getCurrentDate(daysMinus: Long = 0): String{
            val currentDate = LocalDateTime.now().minusDays(daysMinus)
            val formatter = DateTimeFormatter.ofPattern(Constants.API_QUERY_DATE_FORMAT)

            return currentDate.format(formatter)
        }
    }
}
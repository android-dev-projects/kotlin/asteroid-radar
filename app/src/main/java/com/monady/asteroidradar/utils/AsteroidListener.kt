package com.monady.asteroidradar.utils

class AsteroidListener(private val clickListener: (asteroidId: Long) -> Unit) {
    fun onCLick(asteroidId: Long) = clickListener(asteroidId)
}